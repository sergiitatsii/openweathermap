## OpenWeatherMap

OpenWeatherMap is a weather application using the current weather information provided by OpenWeatherMap http://openweathermap.org. The project includes RxSwift, RxCocoa and SwiftyJSON for a better handling of the JSON data returned by the OpenWeatherMap API.

### Compiling

You need to install dependencies from CocoaPods:

```bash
pod install
```

Open OpenWeatherMap.xcworkspace, and compile.
