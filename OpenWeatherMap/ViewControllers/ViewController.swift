import UIKit
import RxSwift
import RxCocoa
import MapKit
import CoreLocation

/// There is no '[weak self]', because there is only one view controller in the app.

class ViewController: UIViewController {
  
  @IBOutlet weak var searchCityName: UITextField!
  @IBOutlet weak var tempLabel: UILabel!
  @IBOutlet weak var humidityLabel: UILabel!
  @IBOutlet weak var iconLabel: UILabel!
  @IBOutlet weak var cityNameLabel: UILabel!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  @IBOutlet weak var geoLocationButton: UIButton!
  @IBOutlet weak var mapButton: UIButton!
  @IBOutlet weak var mapView: MKMapView!
  
  let disposeBag = DisposeBag()
  
  let locationManager = CLLocationManager()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    style()
    
    let currentLocation = locationManager.rx.didUpdateLocations
      .map { locations in
        return locations[0]
      }
      .filter { location in
        return location.horizontalAccuracy < kCLLocationAccuracyHundredMeters
    }
    
    let geoInput = geoLocationButton.rx.tap.asObservable()
      .do(onNext: {
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
      })
    
    let geoLocation = geoInput.flatMap {
      return currentLocation.take(1)
    }
    
    let geoSearch = geoLocation.flatMap { location in
      return API.shared.currentWeather(lat:
        location.coordinate.latitude, lon: location.coordinate.longitude)
        .catchErrorJustReturn(API.Weather.dummy)
    }
    
    let searchInput = searchCityName.rx.controlEvent(.editingDidEndOnExit).asObservable()
      .map { self.searchCityName.text }
      .filter { ($0 ?? "").count > 0 }
    
    let textSearch = searchInput.flatMap { text in
      return API.shared.currentWeather(city: text ?? "Error")
        .catchErrorJustReturn(API.Weather.dummy)
    }
    
    let mapInput = mapView.rx.regionDidChangeAnimated
      .skip(1)
      .map { _ in self.mapView.centerCoordinate }
    
    let mapSearch = mapInput.flatMap { coordinate in
      return API.shared.currentWeather(lat: coordinate.latitude, lon: coordinate.longitude)
        .catchErrorJustReturn(API.Weather.dummy)
    }
    
    let search = Observable.from([geoSearch, textSearch, mapSearch])
      .merge()
      .asDriver(onErrorJustReturn: API.Weather.dummy)
    
    let running = Observable.from([
      searchInput.map { _ in true },
      geoInput.map { _ in true },
      mapInput.map { _ in true},
      search.map { _ in false }.asObservable()
      ])
      .merge()
      .startWith(true)
      .asDriver(onErrorJustReturn: false)
    
    running
      .skip(1)
      .drive(activityIndicator.rx.isAnimating)
      .disposed(by: disposeBag)
    
    running
      .drive(tempLabel.rx.isHidden)
      .disposed(by: disposeBag)
    
    running
      .drive(iconLabel.rx.isHidden)
      .disposed(by: disposeBag)
    
    running
      .drive(humidityLabel.rx.isHidden)
      .disposed(by: disposeBag)
    
    running
      .drive(cityNameLabel.rx.isHidden)
      .disposed(by: disposeBag)

    search.map { "\($0.temperature)° C" }
      .drive(tempLabel.rx.text)
      .disposed(by: disposeBag)

    search.map { $0.icon }
      .drive(iconLabel.rx.text)
      .disposed(by: disposeBag)

    search.map { "\($0.humidity)%" }
      .drive(humidityLabel.rx.text)
      .disposed(by: disposeBag)

    search.map { $0.cityName }
      .drive(cityNameLabel.rx.text)
      .disposed(by: disposeBag)
    
    // Receive all the non-handled calls from RxMKMapViewDelegateProxy
    mapView.rx.setDelegate(self)
      .disposed(by: disposeBag)
    
    mapButton.rx.tap
      .subscribe(onNext: {
        self.mapView.isHidden = !self.mapView.isHidden
      })
      .disposed(by: disposeBag)
    
    search.map { [$0.overlay()] }
      .drive(mapView.rx.overlays)
      .disposed(by: disposeBag)
    
    let textOrGeoSearch = Observable.from([textSearch, geoSearch])
      .merge()
      .asDriver(onErrorJustReturn: API.Weather.dummy)
    
    textOrGeoSearch
      .map { $0.coordinate }.drive(mapView.rx.location)
      .disposed(by: disposeBag)
    
    mapInput.flatMap { coordinate in
      return API.shared.currentWeatherAround(lat: coordinate.latitude, lon: coordinate.longitude)
        .catchErrorJustReturn([])
      }
      .asDriver(onErrorJustReturn:[])
      .map { $0.map { $0.overlay() } }
      .drive(mapView.rx.overlays)
      .disposed(by: disposeBag)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    Appearance.applyBottomLine(to: searchCityName)
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
  
  // MARK: - Style
  private func style() {
    view.backgroundColor = UIColor.aztec
    searchCityName.textColor = UIColor.ufoGreen
    tempLabel.textColor = UIColor.cream
    humidityLabel.textColor = UIColor.cream
    iconLabel.textColor = UIColor.cream
    cityNameLabel.textColor = UIColor.cream
  }
  
}

extension ViewController: MKMapViewDelegate {
  func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
      if let overlay = overlay as? API.Weather.Overlay {
        let overlayView = API.Weather.OverlayView(overlay: overlay, overlayIcon: overlay.icon)
        return overlayView
      }
      return MKOverlayRenderer()
  }
}
