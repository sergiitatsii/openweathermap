import Foundation
import MapKit
import RxSwift
import RxCocoa

extension MKMapView: HasDelegate {
  public typealias Delegate = MKMapViewDelegate
}

class RxMKMapViewDelegateProxy:
  DelegateProxy<MKMapView, MKMapViewDelegate>,
  DelegateProxyType,
  MKMapViewDelegate {
  
  /// Type of parent object
  public weak private(set) var mapView: MKMapView?
  
  /// Init with ParentObject
  public init(parentObject: ParentObject) {
    mapView = parentObject
    super.init(parentObject: parentObject, delegateProxy: RxMKMapViewDelegateProxy.self)
  }
  
  /// Register self to known implementations
  static func registerKnownImplementations() {
    self.register { RxMKMapViewDelegateProxy(parentObject: $0) }
  }
  
  /// Gets the current `MKMapViewDelegate` on `MKMapView`
  open class func currentDelegate(for object: ParentObject) -> MKMapViewDelegate? {
    return object.delegate
  }
  
  /// Set the MKMapViewDelegate for `MKMapView`
  open class func setCurrentDelegate(_ delegate: MKMapViewDelegate?, to object: ParentObject) {
    object.delegate = delegate
  }
  
}

extension Reactive where Base: MKMapView {
  
  public var delegate: DelegateProxy<MKMapView, MKMapViewDelegate> {
    return RxMKMapViewDelegateProxy.proxy(for: base)
  }
  /// Forward the delegate methods that don’t have a wrapper in the Rx proxy
  public func setDelegate(_ delegate: MKMapViewDelegate) -> Disposable {
    return RxMKMapViewDelegateProxy.installForwardDelegate(
      delegate,
      retainDelegate: false,
      onProxyForObject: base
    )
  }
  
  var overlays: Binder<[MKOverlay]> {
    return Binder(self.base) { mapView, overlays in
      mapView.removeOverlays(mapView.overlays)
      mapView.addOverlays(overlays)
    }
  }
  
  public var regionDidChangeAnimated: ControlEvent<Bool> {
    let source = delegate
      .methodInvoked(#selector(MKMapViewDelegate.mapView(_:regionDidChangeAnimated:)))
      .map { parameters in
        return (parameters[1] as? Bool) ?? false
    }
    return ControlEvent(events: source)
  }
  
  public var location: Binder<CLLocationCoordinate2D> {
    return Binder(self.base) { mapView, location in
//      let span = MKCoordinateSpan(latitudeDelta: 2, longitudeDelta: 2)
//      mapView.region = MKCoordinateRegion(center: location, span: span)
      var region = mapView.region
      region.center = location
      mapView.setRegion(region, animated: true)
    }
  }
  
}
