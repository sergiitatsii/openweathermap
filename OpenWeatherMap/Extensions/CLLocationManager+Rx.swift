import Foundation
import CoreLocation
import RxSwift
import RxCocoa

extension CLLocationManager: HasDelegate {
  public typealias Delegate = CLLocationManagerDelegate
}

class RxCLLocationManagerDelegateProxy:
  DelegateProxy<CLLocationManager, CLLocationManagerDelegate>,
  CLLocationManagerDelegate,
  DelegateProxyType {
  
  /// Type of parent object
  public weak private(set) var manager: CLLocationManager?

  /// Init with ParentObject
  public init(parentObject: ParentObject) {
    manager = parentObject
    super.init(parentObject: parentObject, delegateProxy: RxCLLocationManagerDelegateProxy.self)
  }

  /// Register self to known implementations
  public static func registerKnownImplementations() {
    self.register { parent -> RxCLLocationManagerDelegateProxy in
      RxCLLocationManagerDelegateProxy(parentObject: parent)
    }
  }

  /// Gets the current `CLLocationManagerDelegate` on `CLLocationManager`
  open class func currentDelegate(for object: ParentObject) -> CLLocationManagerDelegate? {
    return object.delegate
  }

  /// Set the CLLocationManagerDelegate for `CLLocationManager`
  open class func setCurrentDelegate(_ delegate: CLLocationManagerDelegate?, to object: ParentObject) {
    object.delegate = delegate
  }
  
}

extension Reactive where Base: CLLocationManager {
  
  var delegate: DelegateProxy<CLLocationManager, CLLocationManagerDelegate> {
    return RxCLLocationManagerDelegateProxy.proxy(for: base)
  }
  
  var didUpdateLocations: Observable<[CLLocation]> {
    return delegate.methodInvoked(#selector(CLLocationManagerDelegate.locationManager(_:didUpdateLocations:)))
      .map { parameters in
        return parameters[1] as! [CLLocation]
    }
  }
  
  var didFailWithError: Observable<Error> {
    return delegate
      .methodInvoked(#selector(CLLocationManagerDelegate.locationManager(_:didFailWithError:)))
      .map { parameters in
        return parameters[1] as! Error
    }
  }
  
}
